### W celu uruchomienia API należy: ###

* zaciągnąć projekt z repozytorium
* uruchomić komendę "composer install"

### Endpointy: ###

* /basket/product/add
* /basket/product/number/increment
* /basket/product/number/decrement
* /basket/product/remove
* /basket/get
* /products/available/get
