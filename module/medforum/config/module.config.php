<?php
return [
    'controllers' => [
        'factories' => [
            'medforum\\V1\\Rpc\\BasketProductAdd\\Controller' => \medforum\V1\Rpc\BasketProductAdd\BasketProductAddControllerFactory::class,
            'medforum\\V1\\Rpc\\BasketProductNumberIncrement\\Controller' => \medforum\V1\Rpc\BasketProductNumberIncrement\BasketProductNumberIncrementControllerFactory::class,
            'medforum\\V1\\Rpc\\BasketProductNumberDecrement\\Controller' => \medforum\V1\Rpc\BasketProductNumberDecrement\BasketProductNumberDecrementControllerFactory::class,
            'medforum\\V1\\Rpc\\BasketProductRemove\\Controller' => \medforum\V1\Rpc\BasketProductRemove\BasketProductRemoveControllerFactory::class,
            'medforum\\V1\\Rpc\\BasketGet\\Controller' => \medforum\V1\Rpc\BasketGet\BasketGetControllerFactory::class,
            'medforum\\V1\\Rpc\\ProductsAvailableGet\\Controller' => \medforum\V1\Rpc\ProductsAvailableGet\ProductsAvailableGetControllerFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'medforum.rpc.basket-product-add' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/basket/product/add',
                    'defaults' => [
                        'controller' => 'medforum\\V1\\Rpc\\BasketProductAdd\\Controller',
                        'action' => 'basketProductAdd',
                    ],
                ],
            ],
            'medforum.rpc.basket-product-number-increment' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/basket/product/number/increment',
                    'defaults' => [
                        'controller' => 'medforum\\V1\\Rpc\\BasketProductNumberIncrement\\Controller',
                        'action' => 'basketProductNumberIncrement',
                    ],
                ],
            ],
            'medforum.rpc.basket-product-number-decrement' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/basket/product/number/decrement',
                    'defaults' => [
                        'controller' => 'medforum\\V1\\Rpc\\BasketProductNumberDecrement\\Controller',
                        'action' => 'basketProductNumberDecrement',
                    ],
                ],
            ],
            'medforum.rpc.basket-product-remove' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/basket/product/remove',
                    'defaults' => [
                        'controller' => 'medforum\\V1\\Rpc\\BasketProductRemove\\Controller',
                        'action' => 'basketProductRemove',
                    ],
                ],
            ],
            'medforum.rpc.basket-get' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/basket/get',
                    'defaults' => [
                        'controller' => 'medforum\\V1\\Rpc\\BasketGet\\Controller',
                        'action' => 'basketGet',
                    ],
                ],
            ],
            'medforum.rpc.products-available-get' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/products/available/get',
                    'defaults' => [
                        'controller' => 'medforum\\V1\\Rpc\\ProductsAvailableGet\\Controller',
                        'action' => 'productsAvailableGet',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'medforum.rpc.basket-product-add',
            1 => 'medforum.rpc.basket-product-number-increment',
            2 => 'medforum.rpc.basket-product-number-decrement',
            3 => 'medforum.rpc.basket-product-remove',
            4 => 'medforum.rpc.basket-get',
            5 => 'medforum.rpc.products-available-get',
        ],
    ],
    'zf-rpc' => [
        'medforum\\V1\\Rpc\\BasketProductAdd\\Controller' => [
            'service_name' => 'BasketProductAdd',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'medforum.rpc.basket-product-add',
        ],
        'medforum\\V1\\Rpc\\BasketProductNumberIncrement\\Controller' => [
            'service_name' => 'BasketProductNumberIncrement',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'medforum.rpc.basket-product-number-increment',
        ],
        'medforum\\V1\\Rpc\\BasketProductNumberDecrement\\Controller' => [
            'service_name' => 'BasketProductNumberDecrement',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'medforum.rpc.basket-product-number-decrement',
        ],
        'medforum\\V1\\Rpc\\BasketProductRemove\\Controller' => [
            'service_name' => 'BasketProductRemove',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'medforum.rpc.basket-product-remove',
        ],
        'medforum\\V1\\Rpc\\BasketGet\\Controller' => [
            'service_name' => 'BasketGet',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'medforum.rpc.basket-get',
        ],
        'medforum\\V1\\Rpc\\ProductsAvailableGet\\Controller' => [
            'service_name' => 'ProductsAvailableGet',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'medforum.rpc.products-available-get',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'medforum\\V1\\Rpc\\BasketProductAdd\\Controller' => 'Json',
            'medforum\\V1\\Rpc\\BasketProductNumberIncrement\\Controller' => 'Json',
            'medforum\\V1\\Rpc\\BasketProductNumberDecrement\\Controller' => 'Json',
            'medforum\\V1\\Rpc\\BasketProductRemove\\Controller' => 'Json',
            'medforum\\V1\\Rpc\\BasketGet\\Controller' => 'Json',
            'medforum\\V1\\Rpc\\ProductsAvailableGet\\Controller' => 'Json',
        ],
        'accept_whitelist' => [
            'medforum\\V1\\Rpc\\BasketProductAdd\\Controller' => [
                0 => 'application/vnd.medforum.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'medforum\\V1\\Rpc\\BasketProductNumberIncrement\\Controller' => [
                0 => 'application/vnd.medforum.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'medforum\\V1\\Rpc\\BasketProductNumberDecrement\\Controller' => [
                0 => 'application/vnd.medforum.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'medforum\\V1\\Rpc\\BasketProductRemove\\Controller' => [
                0 => 'application/vnd.medforum.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'medforum\\V1\\Rpc\\BasketGet\\Controller' => [
                0 => 'application/vnd.medforum.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
            'medforum\\V1\\Rpc\\ProductsAvailableGet\\Controller' => [
                0 => 'application/vnd.medforum.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
        ],
        'content_type_whitelist' => [
            'medforum\\V1\\Rpc\\BasketProductAdd\\Controller' => [
                0 => 'application/vnd.medforum.v1+json',
                1 => 'application/json',
            ],
            'medforum\\V1\\Rpc\\BasketProductNumberIncrement\\Controller' => [
                0 => 'application/vnd.medforum.v1+json',
                1 => 'application/json',
            ],
            'medforum\\V1\\Rpc\\BasketProductNumberDecrement\\Controller' => [
                0 => 'application/vnd.medforum.v1+json',
                1 => 'application/json',
            ],
            'medforum\\V1\\Rpc\\BasketProductRemove\\Controller' => [
                0 => 'application/vnd.medforum.v1+json',
                1 => 'application/json',
            ],
            'medforum\\V1\\Rpc\\BasketGet\\Controller' => [
                0 => 'application/vnd.medforum.v1+json',
                1 => 'application/json',
            ],
            'medforum\\V1\\Rpc\\ProductsAvailableGet\\Controller' => [
                0 => 'application/vnd.medforum.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'zf-content-validation' => [
        'medforum\\V1\\Rpc\\BasketProductAdd\\Controller' => [
            'input_filter' => 'medforum\\V1\\Rpc\\BasketProductAdd\\Validator',
        ],
        'medforum\\V1\\Rpc\\BasketProductNumberIncrement\\Controller' => [
            'input_filter' => 'medforum\\V1\\Rpc\\BasketProductNumberIncrement\\Validator',
        ],
        'medforum\\V1\\Rpc\\BasketProductNumberDecrement\\Controller' => [
            'input_filter' => 'medforum\\V1\\Rpc\\BasketProductNumberDecrement\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'medforum\\V1\\Rpc\\BasketProductAdd\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'productUuid',
                'description' => 'Product UUID',
                'field_type' => 'string',
                'error_message' => 'The product UUID is required!',
            ],
        ],
        'medforum\\V1\\Rpc\\BasketProductNumberIncrement\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'productUuid',
                'description' => 'Product UUID',
                'field_type' => 'string',
                'error_message' => 'The product UUID is required!',
            ],
        ],
        'medforum\\V1\\Rpc\\BasketProductNumberDecrement\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\NotEmpty::class,
                        'options' => [],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Zend\Filter\StringTrim::class,
                        'options' => [],
                    ],
                ],
                'name' => 'productUuid',
                'description' => 'Product UUID',
                'field_type' => 'string',
                'error_message' => 'The product UUID is required!',
            ],
        ],
    ],
    'zf-mvc-auth' => [
        'authorization' => [
            'medforum\\V1\\Rpc\\ProductsAvailableGet\\Controller' => [
                'actions' => [
                    'ProductsAvailableGet' => [
                        'GET' => false,
                        'POST' => false,
                        'PUT' => false,
                        'PATCH' => false,
                        'DELETE' => false,
                    ],
                ],
            ],
        ],
    ],
];
