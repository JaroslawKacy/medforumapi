<?php
namespace medforum\V1\Rpc\BasketProductAdd;

use Interop\Container\ContainerInterface;

use Application\Service\BasketService;

class BasketProductAddControllerFactory
{
    public function __invoke(ContainerInterface $container) 
    {
        return new BasketProductAddController(
            $container->get(BasketService::class)
        );
    }
}
