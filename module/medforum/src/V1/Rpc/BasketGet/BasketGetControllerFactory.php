<?php
namespace medforum\V1\Rpc\BasketGet;

use Interop\Container\ContainerInterface;

use Application\Service\BasketService;

class BasketGetControllerFactory
{
    public function __invoke(ContainerInterface $container) 
    {
        return new BasketGetController(
            $container->get(BasketService::class)
        );
    }
}
