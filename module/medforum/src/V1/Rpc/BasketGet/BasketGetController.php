<?php
namespace medforum\V1\Rpc\BasketGet;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

use Application\Service\BasketService;

class BasketGetController extends AbstractActionController
{
    /**
     * @var \Application\Service\BasketService $basketService
     */
    private $basketService;

    /**
     * @param \Application\Service\BasketService $basketService
     * @return void
     */
    public function __construct(BasketService $basketService)
    {
        $this->basketService = $basketService;
    }

    public function basketGetAction()
    {
        return new JsonModel(
            $this->basketService->getBasket()
        );
    }
}
