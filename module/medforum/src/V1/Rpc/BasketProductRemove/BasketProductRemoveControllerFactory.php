<?php
namespace medforum\V1\Rpc\BasketProductRemove;

use Interop\Container\ContainerInterface;

use Application\Service\BasketService;

class BasketProductRemoveControllerFactory
{
    public function __invoke(ContainerInterface $container) 
    {
        return new BasketProductRemoveController(
            $container->get(BasketService::class)
        );
    }
}
