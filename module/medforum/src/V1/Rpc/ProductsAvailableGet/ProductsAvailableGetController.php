<?php
namespace medforum\V1\Rpc\ProductsAvailableGet;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

use Application\Service\ProductService;

class ProductsAvailableGetController extends AbstractActionController
{
    /**
     * @var \Application\Service\ProductService $productService
     */
    private $productService;

    /**
     * @param \Application\Service\ProductService $productService
     * @return void
     */
    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function productsAvailableGetAction()
    {
$this->response->getHeaders()->addHeaders([
    'Access-Control-Allow-Origin' => '*',
]);
        
        return new JsonModel(
            $this->productService->getAvailableProducts()
        );
    }
}
