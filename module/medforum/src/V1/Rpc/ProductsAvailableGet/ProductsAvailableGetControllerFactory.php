<?php
namespace medforum\V1\Rpc\ProductsAvailableGet;

use Interop\Container\ContainerInterface;

use Application\Service\ProductService;

class ProductsAvailableGetControllerFactory
{
    public function __invoke(ContainerInterface $container) 
    {
        return new ProductsAvailableGetController(
            $container->get(ProductService::class)
        );
    }
}
