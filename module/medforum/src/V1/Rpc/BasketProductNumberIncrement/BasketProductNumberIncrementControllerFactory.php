<?php
namespace medforum\V1\Rpc\BasketProductNumberIncrement;

use Interop\Container\ContainerInterface;

use Application\Service\BasketService;

class BasketProductNumberIncrementControllerFactory
{
    public function __invoke(ContainerInterface $container) 
    {
        return new BasketProductNumberIncrementController(
            $container->get(BasketService::class)
        );
    }
}
