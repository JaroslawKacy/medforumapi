<?php
namespace medforum\V1\Rpc\BasketProductNumberDecrement;

use Zend\Json\Json;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

use Application\Service\BasketService;

class BasketProductNumberDecrementController extends AbstractActionController
{
    /**
     * @var \Application\Service\BasketService $basketService
     */
    private $basketService;

    /**
     * @param \Application\Service\BasketService $basketService
     * @return void
     */
    public function __construct(BasketService $basketService)
    {
        $this->basketService = $basketService;
    }

    public function basketProductNumberDecrementAction()
    {
        $content = Json::decode(
            $this->request->getContent()
        );

        return new JsonModel(
            (array) $this->basketService->decrement($content->productUuid)
        );
    }
}
