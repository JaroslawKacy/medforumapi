<?php
namespace medforum\V1\Rpc\BasketProductNumberDecrement;

use Interop\Container\ContainerInterface;

use Application\Service\BasketService;

class BasketProductNumberDecrementControllerFactory
{
    public function __invoke(ContainerInterface $container) 
    {
        return new BasketProductNumberDecrementController(
            $container->get(BasketService::class)
        );
    }
}
