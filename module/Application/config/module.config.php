<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014-2016 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Application;

use Zend\ServiceManager\Factory\InvokableFactory;

use Application\Service\Factory\BasketServiceFactory;
use Application\Service\Factory\ProductServiceFactory;
use Application\Service\BasketService;
use Application\Service\ProductService;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => 'Literal',
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],

    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
        ],
    ],

    'service_manager' => [
        'factories' => [
            BasketService::class    => BasketServiceFactory::class,
            ProductService::class   => ProductServiceFactory::class,
        ],
    ],

    'available_products' => [
      [
        'uuid'      => 'p1',
        'name'      => 'Product 1',
        'number'    => 5,
        'photo'     => 'floppydisk',
        'price'     => '15 zł',
      ],
      [
        'uuid'      => 'p2',
        'name'      => 'Product 2',
        'number'    => 8,
        'photo'     => 'iphone',
        'price'     => '10 zł',
      ],
      [
        'uuid'      => 'p3',
        'name'      => 'Product 3',
        'number'    => 7,
        'photo'     => 'laptop',
        'price'     => '8 zł',
      ],
      [
        'uuid'      => 'p4',
        'name'      => 'Product 4',
        'number'    => 10,
        'photo'     => 'pendrive',
        'price'     => '12 zł',
      ],
      [
        'uuid'      => 'p5',
        'name'      => 'Product 5',
        'number'    => 12,
        'photo'     => 'pixel',
        'price'     => '25 zł',
      ],
      [
        'uuid'      => 'p6',
        'name'      => 'Product 6',
        'number'    => 3,
        'photo'     => 'tablet',
        'price'     => '100 zł',
      ],
    ],

    'basket' => [
        
    ],

    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
