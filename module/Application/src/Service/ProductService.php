<?php
namespace Application\Service;

use Zend\Json\Json;

class ProductService
{
    const DB_PRODUCT_MOCK   = 'db_product_mock.txt';
    const ACTION_DECREMENT  = 'decrement';
    const ACTION_INCREMENT  = 'increment';

    /**
     * @var array
     */
    private $availableProducts;

    /**
     * @param array $availableProducts
     */
    public function __construct($availableProducts)
    {
        $this->setMockedDb($availableProducts);
    }

    /**
     * @return array
     */
    public function getAvailableProducts()
    {
        $result = [];

        foreach ($this->availableProducts as $availableProduct) {
            $result[] = $availableProduct;
        }

        return $result;
    }

    /**
     * @param string $uuid
     * @return array
     */
    public function getAvailableProduct($uuid)
    {
        $result = null;

        foreach ($this->availableProducts as $availableProduct) {
            if ($availableProduct->uuid === $uuid) {
                $result = clone $availableProduct;

                break;
            }
        }

        return $result;
    }

    /**
     * @param string $uuid
     * @param number $number
     * @param string $action
     * @return void
     */
    public function changeNumberOfAvailableProduct($uuid, $number, $action)
    {
        foreach ($this->availableProducts as $index => $availableProduct) {
            if ($availableProduct->uuid === $uuid) {
                switch ($action) {
                    case self::ACTION_INCREMENT:
                        $this->availableProducts[$index]->number = $this->availableProducts[$index]->number + $number;

                        break;
                    case self::ACTION_DECREMENT:
                        $this->availableProducts[$index]->number = $this->availableProducts[$index]->number - $number;

                        break;
                }

                $this->mockedDbChangeState($this->availableProducts);

                break;
            }
        }
    }

    /**
     * @param array $availableProducts
     * @return void
     */
    private function setMockedDb($availableProducts) {
        if (!file_exists(self::DB_PRODUCT_MOCK)) {
            file_put_contents(self::DB_PRODUCT_MOCK, Json::encode($availableProducts));

            $this->availableProducts = $availableProducts;
        } else {
            $this->availableProducts = Json::decode(file_get_contents(self::DB_PRODUCT_MOCK));
        }
    }

    /**
     * @param array $availableProducts
     * @return void
     */
    private function mockedDbChangeState($availableProducts) {
        file_put_contents(self::DB_PRODUCT_MOCK, Json::encode($availableProducts));
    }
}
