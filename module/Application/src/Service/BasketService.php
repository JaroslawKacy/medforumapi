<?php
namespace Application\Service;

use Zend\Json\Json;

use Application\Service\ProductService;

class BasketService
{
    const DB_BASKET_MOCK    = 'db_basket_mock.txt';
    const ACTION_DECREMENT  = 'decrement';
    const ACTION_INCREMENT  = 'increment';

    /**
     * @var array
     */
    private $basket;

    /**
     * @var \Application\Service\ProductService
     */
    private $productService;

    /**
     * @param array                                 $basket
     * @param \Application\Service\ProductService   $productService
     */
    public function __construct($basket, ProductService $productService)
    {
        $this->productService = $productService;

        $this->setMockedDb($basket);
    }

    /**
     * @return array
     */
    public function getBasket()
    {
        return (array) $this->basket;
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function add($productUuid)
    {
        $initNumber = 1;
        $isInBasket = false;

        foreach ($this->getBasket() as $productInBasket) {
            if ($productInBasket->uuid === $productUuid) {
                $isInBasket = true;

                break;
            }
        }

        if (!$isInBasket) {
            $addedProduct = $this->productService->getAvailableProduct($productUuid);
            $addedProduct->number = $initNumber;

            $this->basket[] = $addedProduct;

            $this->mockedDbChangeState($this->basket);

            $this->productService->changeNumberOfAvailableProduct(
                $productUuid,
                $initNumber,
                ProductService::ACTION_DECREMENT
            );
        }

        $result = new \stdClass();
        $result->products           = $this->productService->getAvailableProducts();
        $result->productsInBasket   = $this->getBasket();

        return $result;
    }

    /**
     * @param string $productUuid
     * @return array
     */
    public function remove($productUuid)
    {
        $availableProduct = $this->productService->getAvailableProduct($productUuid);

        $basket = array_filter($this->getBasket(), function($element) use ($productUuid, $availableProduct) {
            if ($availableProduct && $element->uuid === $productUuid) {
                $this->productService->changeNumberOfAvailableProduct(
                    $productUuid,
                    $element->number,
                    ProductService::ACTION_INCREMENT
                );
            }

            return $element->uuid !== $productUuid;
        });
        
        $this->mockedDbChangeState($basket);

        $this->basket = array_values($basket);

        
        $result = new \stdClass();
        $result->products           = $this->productService->getAvailableProducts();
        $result->productsInBasket   = $this->getBasket();

        return $result;
    }

    /**
     * @param string $productUuid
     * @return \stdClass
     */
    public function decrement($productUuid)
    {
        $inBasketProducts = array_filter($this->getBasket(), function($element) use ($productUuid) {
            return $element->uuid === $productUuid;
        });

        if (count($inBasketProducts) > 0 && array_values($inBasketProducts)[0]->number > 1) {
            $this->productService->changeNumberOfAvailableProduct(
                $productUuid,
                1,
                ProductService::ACTION_INCREMENT
            );

            $this->changeNumberOfProductInBasket($productUuid, self::ACTION_DECREMENT);
        }

        $result = new \stdClass();
        $result->products           = $this->productService->getAvailableProducts();
        $result->productsInBasket   = $this->getBasket();

        return $result;
    }

    /**
     * @param string $productUuid
     * @return \stdClass
     */
    public function increment($productUuid)
    {
        $availableProduct = $this->productService->getAvailableProduct($productUuid);

        if ($availableProduct && $availableProduct->number > 0) {
            $this->productService->changeNumberOfAvailableProduct(
                $productUuid,
                1,
                ProductService::ACTION_DECREMENT
            );

            $this->changeNumberOfProductInBasket($productUuid, self::ACTION_INCREMENT);
        }

        $result = new \stdClass();
        $result->products           = $this->productService->getAvailableProducts();
        $result->productsInBasket   = $this->getBasket();

        return $result;
    }

    /**
     * @param string $uuid
     * @param number $number
     * @param string $action
     * @return void
     */
    public function changeNumberOfProductInBasket($uuid, $action)
    {
        $basket = $this->getBasket();
 
        foreach ($basket as $index => $product) {
            if ($product->uuid === $uuid) {
                switch ($action) {
                    case self::ACTION_INCREMENT:
                        $basket[$index]->number = $basket[$index]->number + 1;

                        break;
                    case self::ACTION_DECREMENT:
                        $basket[$index]->number = $basket[$index]->number - 1;

                        break;
                }

                $this->mockedDbChangeState($basket);

                break;
            }
        }
    }

    /**
     * @param array $basket
     * @return void
     */
    private function setMockedDb($basket) {
        if (!file_exists(self::DB_BASKET_MOCK)) {
            file_put_contents(self::DB_BASKET_MOCK, Json::encode($basket));

            $this->basket = $basket;
        } else {
            $products = (array) Json::decode(file_get_contents(self::DB_BASKET_MOCK));

            foreach ($products as $product) {
                $this->basket[] = $product;
            }
        }
    }

    /**
     * @param array $basket
     * @return void
     */
    private function mockedDbChangeState($basket) {
        file_put_contents(self::DB_BASKET_MOCK, Json::encode($basket));
    }
}
