<?php
namespace Application\Service\Factory;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

use Application\Service\BasketService;
use Application\Service\ProductService;

class BasketServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new BasketService(
            $container->get('config')['basket'],
            $container->get(ProductService::class)
        );
    }
}
